import { ChildProcess } from "child_process";
import { useContext } from "react";
import { useLocation, Navigate } from "react-router-dom";
import GlobalContext from "./context/GlobalContextProvider";

const RequireAuth =({children}:any)=>{
    const { userName }: any = useContext(GlobalContext);

    const location = useLocation();

    return (   
        userName ? children : <Navigate to="/" state={{from:location}} replace />

    );
}
export default RequireAuth;