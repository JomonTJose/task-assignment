import { render, screen, waitFor } from "@testing-library/react";
import BoardSelector from '../components/BoardSelector';
import { BrowserRouter as Router } from 'react-router-dom';
import * as router from 'react-router'
import boardService from "../services/board.service";
import { GlobalContextValuesProvider } from "../context/GlobalContextProvider";
import { act } from "react-dom/test-utils";

const navigate = jest.fn()

beforeEach(() => {
    jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})

fdescribe("BoardSelector", () => {

    let originFetch: any;
    beforeEach(() => {
        originFetch = (global as any).fetch;
    });
    afterEach(() => {
        (global as any).fetch = originFetch;
    });

    it("Should render Board Selector Component", async () => {
        const fakeResponse = [
            {
                "id": "en",
                "name": "England"
            },
            {
                "id": "de",
                "name": "Deutsch"
            },
            {
                "id": "it",
                "name": "Italiano"
            }
        ];

        const mockBoards = jest.spyOn(boardService, "getAllBoards").mockResolvedValue(fakeResponse);

        const mRes = { json: jest.fn().mockResolvedValueOnce(mockBoards) };
        const mockedFetch = jest.fn().mockResolvedValueOnce(mRes as any);
        (global as any).fetch = mockedFetch;
        const setSelectedBoard =()=>{return {"id":"en", "name":"English"}}

        await act(async ()=>{
            render(

                <GlobalContextValuesProvider value={setSelectedBoard}>
                    <Router>
                        <BoardSelector />
                    </Router>
                </GlobalContextValuesProvider>
    
            );
        })
      
        const linkElement = screen.getByTestId("0divContainer");
        //const div = waitForElement(() => screen.getByTestId('divContainer'));
        expect(linkElement).toBeInTheDocument();
    })

    it("Should test Board Select button click", async () => {
        const fakeResponse = [
            {
                "id": "en",
                "name": "England"
            },
            {
                "id": "de",
                "name": "Deutsch"
            },
            {
                "id": "it",
                "name": "Italiano"
            }
        ];

        const mockBoards = jest.spyOn(boardService, "getAllBoards").mockResolvedValue(fakeResponse);

        const mRes = { json: jest.fn().mockResolvedValueOnce(mockBoards) };
        const mockedFetch = jest.fn().mockResolvedValueOnce(mRes as any);
        (global as any).fetch = mockedFetch;
        const setSelectedBoard =()=>{return {"id":"en", "name":"English"}}

        await act(async ()=>{
            render(

                <GlobalContextValuesProvider value={setSelectedBoard}>
                    <Router>
                        <BoardSelector />
                    </Router>
                </GlobalContextValuesProvider>
    
            );
        })
      await waitFor(() => {
        const linkElement = screen.getByTestId("btn0");
        linkElement.click();
        expect(navigate).toBeCalledWith("/home");
      });
       
    })
})
