import { fireEvent, render, screen } from "@testing-library/react";
import Login from '../components/Login';
import { BrowserRouter as Router } from 'react-router-dom';
import * as router from 'react-router'
import { GlobalContextValuesProvider } from "../context/GlobalContextProvider";


const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})

describe("Login Component", () => {

    test('renders learn react link', () => {
        render(
            <GlobalContextValuesProvider value ={null}>
                <Router>
                    <Login />
                </Router>
            </GlobalContextValuesProvider>


        );
        const linkElement = screen.getByTestId("setEmail");
        expect(linkElement).toBeInTheDocument();
    });

    test('Login Button Click', () => {
        render(
            <GlobalContextValuesProvider value ={null}>
            <Router>
                <Login />
            </Router>
        </GlobalContextValuesProvider>

        );
        const btnLogin = screen.getByTestId("submit");
        btnLogin.click();
        expect(navigate).toBeCalledWith("/board");

    });

    test('Login Button Click', () => {
        const userName= {userName:"dummy"}
        render(
            <GlobalContextValuesProvider value ={userName}>
            <Router>
                <Login />
            </Router>
        </GlobalContextValuesProvider>


        );
        const inputText = screen.getByTestId("setEmail");
        fireEvent.change(inputText, {target:{value: 'dummy'}})
        expect(inputText).toHaveDisplayValue('dummy')
    });

})