import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import Home from '../components/Home';
import { BrowserRouter as Router } from 'react-router-dom';
import * as router from 'react-router'
import boardService from "../services/board.service";
import { GlobalContextValuesProvider } from "../context/GlobalContextProvider";
import { act } from "react-dom/test-utils";
import { wait } from "@testing-library/user-event/dist/utils";

const navigate = jest.fn()

beforeEach(() => {
    jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
});


const fakeResponse = {
    "drafts": [
        {
            "id": "21779a83-65d2-cb7c-cd33-b09ac7a83d3e",
            "boardId": "en",
            "author": "uhuu@upday.com",
            "title": "new title",
            "description": "big description",
            "imageURL": "https://www.upday.com/wp-content/themes/upday/images/upday-logo-black.svg",
            "status": "draft",
            "CreatedAt": "2022-07-30T17:28:11.5829571Z"
        },
        {
            "id": "485632b9-8543-0526-ed53-e78494308498",
            "boardId": "en",
            "author": "jose.jomon@live.com",
            "title": "New News",
            "description": "Sample ",
            "imageURL": "http://google.com/a.jpg",
            "status": "draft",
            "CreatedAt": "2022-07-31T07:24:54.6186418Z"
        }
    ],
    "published": [
        {
            "id": "0c493987-b4a1-55fe-e0b0-8ef4b23d68d4",
            "boardId": "en",
            "author": "uhuu@upday.com",
            "title": "new title",
            "description": "big description",
            "imageURL": "https://www.upday.com/wp-content/themes/upday/images/upday-logo-black.svg",
            "status": "published",
            "CreatedAt": "2022-07-30T17:26:25.6790474Z"
        }
    ],
    "archives": []
};


describe("Home Component", () => {

    let originFetch: any;
    beforeEach(() => {
        originFetch = (global as any).fetch;
    });
    afterEach(() => {
        (global as any).fetch = originFetch;
    });

    it("Should render Home Component", async () => {
        const mockBoards = jest.spyOn(boardService, "getNewsbyBoardId").mockResolvedValue(fakeResponse);

        const mRes = { json: jest.fn().mockResolvedValueOnce(mockBoards) };
        const mockedFetch = jest.fn().mockResolvedValueOnce(mRes as any);
        (global as any).fetch = mockedFetch;

        const setSelectedBoard = () => { return { "id": "en", "name": "English" } }

        await act(async () => {
            render(

                <GlobalContextValuesProvider value={setSelectedBoard}>
                    <Router>
                        <Home />
                    </Router>
                </GlobalContextValuesProvider>

            );
        })

        const linkElement = screen.getByTestId("homeSection");
        //const div = waitForElement(() => screen.getByTestId('divContainer'));
        expect(linkElement).toBeInTheDocument();
    });
    it("Should render Home Component", async () => {
        const mockBoards = jest.spyOn(boardService, "getNewsbyBoardId").mockResolvedValue(fakeResponse);
        const mRes = { json: jest.fn().mockResolvedValueOnce(mockBoards) };
        const mockedFetch = jest.fn().mockResolvedValueOnce(mRes as any);
        (global as any).fetch = mockedFetch;
        const setSelectedBoard = () => { return { "id": "en", "name": "English" } }

        await act(async () => {
            render(

                <GlobalContextValuesProvider value={setSelectedBoard}>
                    <Router>
                        <Home />
                    </Router>
                </GlobalContextValuesProvider>

            );
        })
        const linkElement = screen.getByTestId("changeBoard");
        linkElement.click();
        expect(navigate).toBeCalledWith("/board");
    });

    it("Should render Create News Component on click of button", async () => {
        const mockBoards = jest.spyOn(boardService, "getNewsbyBoardId").mockResolvedValue(fakeResponse);
        const mRes = { json: jest.fn().mockResolvedValueOnce(mockBoards) };
        const mockedFetch = jest.fn().mockResolvedValueOnce(mRes as any);
        (global as any).fetch = mockedFetch;
        const setSelectedBoard = () => { return { "id": "en", "name": "English" } }

        await act(async () => {
            render(

                <GlobalContextValuesProvider value={setSelectedBoard}>
                    <Router>
                        <Home />
                    </Router>
                </GlobalContextValuesProvider>

            );
        })
        const linkElement = screen.getByTestId("addNewsBtn");
        fireEvent.click(linkElement);
       
        expect(screen.getByTestId('addNewsContainer')).toBeInTheDocument();
    });

    
});