import React from 'react';
import './index.css';
import { Routes, Route } from "react-router-dom";
import Login from './components/Login';
import BoardSelector from './components/BoardSelector';
import Home from './components/Home';
import RequireAuth from './RequireAuth';
import ErrorComponent from './components/ErrorComponent';

const App = () => {

  return (
    <main>
      <div className="App">
        <Routes>
          <Route
            path='board'
            element=
            {
              <RequireAuth>
                <BoardSelector />
              </RequireAuth>
            }
          />
          <Route
            path='home'
            element=
            {
              <RequireAuth>
                <Home />
              </RequireAuth>
            }
          />
          <Route path="/" element={<Login />} />
          <Route path="/error" element={<ErrorComponent/>}/>
        </Routes>
         </div>
    </main>
  );
}
export default App;