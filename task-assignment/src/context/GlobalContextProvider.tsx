import { createContext, useState } from "react";

const GlobalContext= createContext({});
export const GlobalContextValuesProvider =({children}:any)=>{
    const [userName, setUserName] = useState('');
    const [selectedBoard, setSelectedBoard] = useState({});
    return (
        <GlobalContext.Provider value={{ userName, setUserName, selectedBoard, setSelectedBoard}}>
            {children}
        </GlobalContext.Provider> 
    )
} 

export default GlobalContext;