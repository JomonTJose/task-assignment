import axios from 'axios';
import { INews } from '../models/news.model';

const BOARD_API_URL = "http://localhost:8080/v1/";

const getAllBoards =async ()=>{
   const response:any = await axios.get(`${BOARD_API_URL}board`);
   console.log(response.data)
    return response.data;
}

const getNewsbyBoardId =async (id:any)=>{
   const response:any =  await axios.get(`${BOARD_API_URL}board/${id}/news`);
   return response;
}
const postNews = async(newsItem: INews)=>{
    console.log(newsItem);
    console.log(`${BOARD_API_URL}news`);
    var response: any =await axios.post(`${BOARD_API_URL}news`, newsItem);
    return response;
}

const publishNews = async(news: INews)=>{
    var response: any =await axios.post(`${BOARD_API_URL}news/${news.id}/published`, news);
    return response;
}

const updateNews = async (news: any) => {
    var response: any = await axios.put(`${BOARD_API_URL}news`,news);
    return response;
}

const boardService ={
    getAllBoards,
    getNewsbyBoardId,
    postNews,
    publishNews,
    updateNews
}

export default boardService;