import React, { useContext, useEffect, useState } from 'react'
import boardService from '../services/board.service';
import { useNavigate } from 'react-router-dom';
import GlobalContext from '../context/GlobalContextProvider';

const BoardSelector = () => {
  const [boards, setBoards]:any = useState();
  const { setSelectedBoard }: any = useContext(GlobalContext)
  const [loading, setLoading] = useState(false);
  let navigate = useNavigate();
  const fetchBoard = async () => {
    setLoading(true);
    try {
      const response: any = await boardService.getAllBoards();
      setBoards(response);
      setLoading(false);

    } catch (error: any) {
      setLoading(false);
      console.error(error);
      navigate("/error" , {state:{errorMessage :error.message}});
    }
  }

  const SetBoard = (selectedBoard: any) => {
    setSelectedBoard(selectedBoard);
    navigate("/home");
  }

  useEffect(() => {
    fetchBoard();
  }, []);


  return (
   
    <section>
       {!loading ? 
          (
          <>
          <h1>Please select a Board</h1>

          {boards?.map((item: any, index: any) => {
            return (
              <li key={index}  data-testid={index+"divContainer"}>
                <button
                  data-testid={"btn"+index}
                  onClick={() => SetBoard(item)}
                  value={item.name}
                >
                  {item.name}
                </button>
              </li>
            )
    
          })}
          </>
          ): "Loading Boards...."
          
       }
    

       

    </section>
  )
}

export default BoardSelector