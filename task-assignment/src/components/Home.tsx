import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { INews } from '../models/news.model';
import GlobalContext from '../context/GlobalContextProvider';
import CreateNews from './CreateNews';
import boardService from '../services/board.service';

interface INewsTypes {
  archives: INews[]
  drafts: INews[]
  published: INews[]
}

const Home = () => {

  const [news, setNews] = useState<INews[] | undefined>();
  const [darftNews, setDraftNews] = useState<INews[] | undefined>();
  const [arcivedNews, setArchivedNews] = useState<INews[] | undefined>();
  const [loading, setLoading] = useState(false);
  const { selectedBoard }: any = useContext(GlobalContext);
  const [modalState, setModalState] = useState(false);
  const navigate = useNavigate();
  const [canEdit, setCanEdit] = useState(false);
  const [editNewsItem, setEditNewItem] = useState<INews>();
  useEffect(() => {
    fetchBoardbyId()
  }, [])

  const fetchBoardbyId = async () => {
    setLoading(true);
    try {
      await boardService.getNewsbyBoardId(selectedBoard.id).then((response) => {
        setNews(response.data?.published);
        setDraftNews(response.data?.drafts);
        setArchivedNews(response.data?.archives);
        setLoading(false);
      })

    } catch (error: any) {
      console.log(error);
      if (error?.code === "ERR_BAD_REQUEST") {
        navigate("/error");
      }
    }
  }

  const handleAddNews = () => {
    setModalState(true);
  }

  const showModal = (e: any) => {
    setModalState(false);
    fetchBoardbyId();
  }

  const publishNews = async (e: any, newsItem: INews) => {
    e.preventDefault();
    if (newsItem.author && newsItem.description && newsItem.imageURL && newsItem.title) {
      await boardService.publishNews(newsItem).then((data) => {
        alert(`News with ID ${newsItem.id} is published successfully`);
        fetchBoardbyId()
      }).catch((error) => {
        console.log(error);

      })
    }
  }

  const goBacktoBoard = (e: any) => {
    e.preventDefault();
    navigate("/board");
  }

  const editNews = (e: any, newsItem: INews) => {
    setEditNewItem(newsItem);
    setCanEdit(true);
    setModalState(true);
  }
  return (
    <section>
    {!loading ? (
      <>
    <section data-testid='homeSection'>
      <button
        onClick={goBacktoBoard}
        data-testid="changeBoard">
        Change NewsBoard
      </button>

      {!modalState
        ? null :
        <CreateNews
          data-testid="openCloseModal"
          onClose={showModal}
          modalState={modalState}
          canEdit={canEdit}
          editNewsItem={editNewsItem}
        />
      }
      <h1 >{selectedBoard.name}</h1>
      <div>
        <button
          data-testid="addNewsBtn"
          onClick={handleAddNews}
        >
          Create News
        </button>
      </div>
      {loading ? "Loading News" :
        <div>
          <div>
            <h2>Main News ({news?.length})</h2>

            {
              news?.map((item, index) => {
                return (
                  <div className='card'>
                    <div className='card-container'>
                      <h2>{item.title}</h2>
                      <p>{item.description}</p>
                    </div>
                    <img src={item.imageURL}/>
                    <div className='float-right'>
                      <button data-testid="editBtn" onClick={(e) => editNews(e, item)}>Edit</button>
                    </div>
                  </div>
                )
              })
            }
          </div>
          <div>
            <h2>Draft ({darftNews?.length})</h2>

            {
              darftNews?.map((item, index) => {
                return (
                  <div className='card'>
                    <div className='card-container'>
                      <h2>{item.title}</h2>
                      <p>{item.description}</p>
                    </div>
                    <img src={item.imageURL} width={300} height={300} />
                    <div className='float-right'>
                      <button  data-testid={"draftEditBtn" +index} onClick={(e) => editNews(e, item)}>Edit</button>
                      <button  data-testid={"publishEditBtn"+index}  onClick={(e) => publishNews(e, item)}>Publish</button>
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
      }
    </section>
    </>
    ): "Please wait.. Loading News"
    }
    </section>
    )
}
export default Home