import { useContext, useState } from 'react'
import '../index.css';
import { useNavigate } from 'react-router-dom';
import GlobalContext from '../context/GlobalContextProvider';

const Login = () => {
  const [email, setEmail] = useState("");
  const {setUserName}:any = useContext(GlobalContext);

  let navigate = useNavigate();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    setUserName(email);
    navigate("/board");
  }



  return (
    <section>

      <section>
        <h1>Sign In</h1>
        <form onSubmit={handleSubmit} data-testid="form">

          <div>
            <label htmlFor="username">Username</label>
            <input
              autoFocus
              type="email"
              id="username"
              autoComplete="off"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              required
              data-testid="setEmail"
            />
            <button data-testid="submit">Sign In</button>
          </div>
        </form>
      </section>
    </section>)

}

export default Login