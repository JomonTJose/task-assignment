import React, { useContext, useState } from 'react'
import { INews } from '../models/news.model'
import Header from './Header';
import '../index.css'
import boardService from '../services/board.service';
import GlobalContext from '../context/GlobalContextProvider';

const CreateNews = ({ modalState, onClose, canEdit, editNewsItem }: any) => {
    const { userName, selectedBoard }: any = useContext(GlobalContext);

    console.log(modalState);
    var boardId = selectedBoard.id;

    const [newsTitle, setnewsTitle] = useState("");
    const [newsContent, setNewsContent] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    let todaysDate = new Date();
    let todaysDateinString = todaysDate.toISOString();
    const handleCreateNews = async (e: any) => {
        e.preventDefault();
        var newNews: INews = {
            author: userName,
            boardId: boardId,
            createdAt: todaysDateinString,
            description: newsContent,
            title: newsTitle,
            imageURL: imageUrl,
            id: Math.random().toString(36).slice(2, 7),
            status: "draft"
        };
        console.log(editNewsItem)
        await boardService.postNews(newNews).then((data) => {
            modalState = false;
            onClose(e);
        }).catch((err) => {
            console.log(err);
        });
    }

    const handleEditNews = async (e: any) => {
        console.log(imageUrl);
        e.preventDefault();
        let todaysDate = new Date();
        let todaysDateinString = todaysDate.toISOString();

        editNewsItem.createdAt = todaysDateinString;
        editNewsItem.description = newsContent ? newsContent : editNewsItem.description  ;
        editNewsItem.title = newsTitle ?newsTitle : editNewsItem.title ;
        editNewsItem.imageURL = imageUrl ? imageUrl :editNewsItem.imageURL ;

        console.log(editNewsItem)
        await boardService.updateNews(editNewsItem).then((data) => {
            alert("News Item edited successfully");
            modalState = false;
            onClose(e);
        }).catch((err) => {
            console.log(err);
        });
    }


    const handleOnClose = (e: any) => {
        modalState = false;
        onClose(e);
    }


    return (
        <div data-testid="addNewsContainer" className="modal" id="myModal">

            {
                !canEdit ? (
                    <div className='news-container'>
                        <Header title={"Dashboard: Create News"} />
                        <form
                            onSubmit={(e: any) => handleCreateNews(e)}
                            onReset={handleOnClose}
                        >
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Title</label>
                                </div>
                                <div className='col-75'>
                                    <input
                                        type="text"
                                        value={newsTitle}
                                        onChange={(e) => { setnewsTitle(e.target.value) }}
                                        autoFocus
                                        required
                                    />
                                </div>

                            </div>
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Description</label>
                                </div>
                                <div className='col-75'>
                                    <textarea
                                        value={newsContent}
                                        onChange={(e) => { setNewsContent(e.target.value) }}
                                        cols={10}
                                        rows={10}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Image Url</label>
                                </div>
                                <div className='col-75'>
                                    <input
                                        type="url"
                                        value={imageUrl}
                                        onChange={(e) => { setImageUrl(e.target.value) }}
                                    />
                                </div>
                            </div>
                            <div className="row float-right">
                                <button type="submit" className='close'>Post News</button>
                                <button type="reset" className='close' style={{ "marginRight": "10px" }} >Cancel</button>
                            </div>
                        </form>
                    </div>
                ) : (
                    <div className='news-container'>
                        <Header title={"Dashboard: Edit News"} />
                        <form
                            onSubmit={(e: any) => handleEditNews(e)}
                            onReset={handleOnClose}
                        >
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Title</label>
                                </div>
                                <div className='col-75'>
                                    <input
                                        type="text"
                                        defaultValue={editNewsItem.title}
                                        onChange={(e) => { setnewsTitle(e.target.value) }}
                                        autoFocus
                                        required
                                    />
                                </div>

                            </div>
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Description</label>
                                </div>
                                <div className='col-75'>
                                    <textarea
                                        defaultValue={editNewsItem.description}
                                        onChange={(e) => { setNewsContent(e.target.value) }}
                                        cols={10}
                                        rows={10}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="row" >
                                <div className='col-25'>
                                    <label>Image Url</label>
                                </div>
                                <div className='col-75'>
                                    <input
                                        type="url"
                                        defaultValue={editNewsItem.imageURL}
                                        onChange={(e) => { setImageUrl(e.target.value) }}
                                    />
                                </div>
                            </div>
                            <div className="row float-right">
                                <button type="submit" className='close'>Save Changes</button>
                                <button type="reset" className='close' style={{ "marginRight": "10px" }} >Cancel</button>
                            </div>
                        </form>
                    </div>
                )
            }


        </div>

    )
}

export default CreateNews