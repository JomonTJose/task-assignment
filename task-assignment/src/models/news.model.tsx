export interface INews{
    id: string;
    boardId: string;
    author: string;
    title: string;
    description: string; 
    imageURL: string;
    createdAt: string;
    status: string;
} 

export enum Status{
    "draft",
    "published",
    "archive"
}