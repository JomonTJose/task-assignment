export default interface IUser {   
    email: string;
    password: string;
    roles?: Array<string>;
    selectedBoard?: string;
} 